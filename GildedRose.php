<?php
namespace GildedRose;
require_once 'vendor/autoload.php';



// use AbstractItem;


class GildedRose
{
    private $items;

    public function __construct($items)
    {
        $this->items = $items;
    }

    public function update_quality()
    {
        foreach ($this->items as $item) {
            $item->UpdateSellIn();
            $item->UpdateQuality();
        }
    }
}
