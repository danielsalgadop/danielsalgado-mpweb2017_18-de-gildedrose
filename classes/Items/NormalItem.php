<?php
namespace GildedRose\Items;
use GildedRose\ItemDecorator;



/**
*
*/
class NormalItem extends ItemDecorator
{
    protected $decorator;
    private $delta_quality = -1;

    public function __construct(ItemDecorator $itemDecorator)
    {
        $this->decorator = $itemDecorator;
    }
    public function calculateDeltaQualityBySellIn()
    {
        $delta_quality = $this->delta_quality;
        if ($this->decorator->item->sell_in < 0) {
            $delta_quality *= 2;
        }
        return $delta_quality;
    }
}
