<?php
namespace GildedRose\Items;
use GildedRose\ItemDecorator;


class TicketItem extends ItemDecorator
{
    protected $decorator;
    private $delta_quality = 1;

    public function __construct(ItemDecorator $itemDecorator)
    {
        $this->decorator = $itemDecorator;
    }
    public function calculateDeltaQualityBySellIn()
    {
        $delta_quality = $this->delta_quality;
        if ($this->decorator->item->sell_in < 0) {
            $delta_quality = - $this->decorator->item->quality;
        } elseif ($this->decorator->item->sell_in < 5) {
            $delta_quality *= 3;
        } elseif ($this->decorator->item->sell_in < 10) {
            $delta_quality *= 2;
        }
        return $delta_quality;
    }
}