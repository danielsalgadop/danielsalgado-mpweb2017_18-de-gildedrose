<?php

namespace GildedRose\Items;
use GildedRose\ItemDecorator;


/*
 */
class LegendaryItem extends ItemDecorator
{
    protected $decorator;
    // private $delta_quality = 0;

    public function __construct(ItemDecorator $itemDecorator)
    {
        $this->decorator = $itemDecorator;
        $this->decrease_sell_in = 0;
    }
    public function UpdateQuality()
    {
    }
}
