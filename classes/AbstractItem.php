<?php
namespace GildedRose;

abstract class AbstractItem
{
    // default values (can be overriden)
    private $max_value = 50;
    private $min_value = 0;
    public $decrease_sell_in = 1; // TODO - revisar el impacto de esta visibilidad en LegendaryItem
    protected $item;
    // No entiendo por que hay que hacer esto. Aunque haga:
    // class ItemDecorator extends Item
    // al quitar este __toString, los valores me aparecen vacios
    public function __toString()
    {
        return (string) $this->decorator->item;
    }
    public function setQualityInCorrectRange($delta)
    {
        $final_quality = $this->decorator->item->quality + $delta;

        if ($final_quality > $this->max_value) {
            $final_quality = $this->max_value;
        } elseif ($final_quality < $this->min_value) {
            $final_quality = $this->min_value;
        }
        $this->decorator->item->quality = $final_quality;
    }
    public function UpdateSellIn()
    {
        $this->decorator->item->sell_in -= $this->decrease_sell_in;
    }
    public function UpdateQuality()
    {
        $delta_quality = $this->calculateDeltaQualityBySellIn();
        $this->setQualityInCorrectRange($delta_quality);
    }
}
