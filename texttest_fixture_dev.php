<?php
// require_once 'vendor/autoload.php';
// use GildedRose\AbstractItem;
require_once 'GildedRose.php';

use GildedRose\ItemDecorator;
use GildedRose\Item;
use GildedRose\Items\NormalItem;
use GildedRose\Items\OlderTheBetterItem;
use GildedRose\Items\LegendaryItem;
use GildedRose\Items\TicketItem;
use GildedRose\Items\ConjuredItem;
use GildedRose\GildedRose;

echo "OMGHAI!\n";
$decorator = new ItemDecorator(new Item('+5 Dexterity Vest', 10, 20));
$dexteriry = new NormalItem($decorator);

$decorator = new ItemDecorator(new Item('Aged Brie', 2, 0));
$aged_brie = new OlderTheBetterItem($decorator);

$decorator = new ItemDecorator(new Item('Elixir of the Mongoose', 5, 7));
$elixir = new NormalItem($decorator);

$decorator = new ItemDecorator(new Item('Sulfuras, Hand of Ragnaros', 0, 80));
$sulfuras_1 = new LegendaryItem($decorator);

$decorator = new ItemDecorator(new Item('Sulfuras, Hand of Ragnaros', -1, 80));
$sulfuras_2 = new LegendaryItem($decorator);

$decorator = new ItemDecorator(new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20));
$ticket_1 = new TicketItem($decorator);

$decorator = new ItemDecorator(new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49));
$ticket_2 = new TicketItem($decorator);

$decorator = new ItemDecorator(new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49));
$ticket_3 = new TicketItem($decorator);
// $

$decorator = new ItemDecorator(new Item('Conjured Mana Cake', 3, 6));
$mana_cake = new ConjuredItem($decorator);

$items = array(
    $dexteriry,
    $aged_brie,
    $elixir,
    $sulfuras_1,
    $sulfuras_2,
    $ticket_1,
    $ticket_2,
    $ticket_3,
    $mana_cake
);

$app = new GildedRose($items);

$days = 2;
if (count($argv) > 1) {
    $days = (int) $argv[1];
}

for ($i = 0; $i < $days; $i++) {
    echo("-------- day $i --------\n");
    echo("name, sellIn, quality\n");
    foreach ($items as $item) {
        echo $item . PHP_EOL;
    }
    echo PHP_EOL;
    $app->update_quality();
}

