rm -rf /tmp/gilded
mkdir /tmp/gilded
php texttest_fixture_dev.php 100 > /tmp/gilded/dev &&
grep Eli /tmp/gilded/dev > /tmp/gilded/Eli-dev &&
grep Dex /tmp/gilded/dev > /tmp/gilded/Dex-dev &&
grep Age /tmp/gilded/dev > /tmp/gilded/Age-dev &&
grep Sulfuras /tmp/gilded/dev > /tmp/gilded/Sulfuras-dev &&
grep Backstage /tmp/gilded/dev > /tmp/gilded/Backstage-dev &&
grep Eli output_100_textest_fixture.php_original_gilded_rose > /tmp/gilded/Eli-ori &&
grep Dex output_100_textest_fixture.php_original_gilded_rose > /tmp/gilded/Dex-ori &&
grep Age output_100_textest_fixture.php_original_gilded_rose > /tmp/gilded/Age-ori &&
grep Sulfuras output_100_textest_fixture.php_original_gilded_rose > /tmp/gilded/Sulfuras-ori &&
grep Backstage output_100_textest_fixture.php_original_gilded_rose > /tmp/gilded/Backstage-ori &&
rm /tmp/gilded/dev &&
md5sum /tmp/gilded/*
