Entregable: Gilded Rose

Este ejercicio se presenta en diferentes enunciados a lo largo de la asignatura y se irá desarrollando de forma incremental. Este entregable tiene dos tareas:

 

Tarea 1

Mediante esta kata (ver el enlace: kata https://en.wikipedia.org/wiki/Kata_(programming) para más información) irás aprendiendo y reforzando los conceptos teóricos aprendidos durante las sesiones.

La kata que vas a realizar se llama Gilded Rose y está diseñada para que apliques conceptos de Diseño Eficiente.

A continuación, se presentan diferentes enlaces explicando el funcionamiento y requerimientos de esta kata.

    Lee el enunciado: Gilded Rose Requirements. https://github.com/emilybache/GildedRose-Refactoring-Kata/blob/master/GildedRoseRequirements.txt
    Puedes encontrar el código en el enlace: Gilded Rose Source Code. https://github.com/vicaba/GildedRose-Refactoring-Kata/tree/master/php/src

 

Recuerda que después de aplicar cambios en el código, el comportamiento debe ser el mismo. No es necesario añadir ninguna funcionalidad.

El proyecto debe estar provisionado con composer y los commits al repositorio deben ser frecuentes con mensajes descriptivos.

El nombre del proyecto será [NombreApellido]-mpweb[año]-de-GildedRose. NombreApellido corresponde al nombre y apellido del alumno y año corresponde al periodo que se cursa el Máster, por ejemplo, al Máster cursado la última mitad del año 2016 y la primera mitad del año 2017 le corresponde año=2016_17.

 

A entregar:

URL del repositorio Gilded Rose. Dar al usuario del mentor acceso de lectura.

 

Tarea 2

Elabora un breve informe (PDF o md en el mismo repositorio) con:

    ¿Qué hace el código a simple vista?
    ¿Te parce un mal código?

o   Justifica por qué (con pequeños ejemplos de código si hace falta)

    ¿Qué harías para mejorarlo? (especificando el orden de los pasos que seguirías)
    Refactoring semántico. Aplica los distintos refactorings y patrones vistos hasta el momento. Énfasis en semántico: nombres de variables, métodos, constantes semánticos.

 

El enunciado es acumulativo para el entregable final.

Una vez ejecutado el enunciado de esta sesión marca el último commit con el tag [semantic]. Recuerda subir el tag al repositorio remoto ya que por defecto al hacer push no se suben los tags.

Para saber cómo crear un tag consulta el enlace: Git Basics Tagging. 

A entregar:

URL del repositorio Gilded Rose apuntando al tag correspondientes al ejercicio de esta sesión.

El nombre del fichero txt a entregar debe ser el mismo que el nombre del repositorio.