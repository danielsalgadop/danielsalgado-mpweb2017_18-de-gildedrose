# Gilded Rose Analisis

¿Qué hace el código a simple vista?



¿Te parce un mal código?
Clase anémica.
Class item parece un Data Transfer Object. No hay lógica algun (salvo convertir en string)
Las propiedades de la clase Item son públicas. Cualquier programador puede leer y escribir en ellas con operador ->. Mejor hacerlas privadas y crear getters y setters

Los Items se identifican por nombre, esto hace dificil que se puedan renombrar. Sería mejor identificarles internamente por un id único. Incluso usar un sistema de base de datos para su almacenaje. Incluso se podría hacer tipos de Items, estilo ('normales', 'no_perecederos', 'conciertos') donde se reflejará su manera en la que la calidad varia en el tiempo. La instancia sería.

```php
new Ìtem('Nombre del producto',$sell_in, $quality, 'TIPO_PRODUCTO')
```

El parámetro $sell_in claramente es una fecha. La almacenaría como propiedad de Item. Y calcularía la diferencia de fecha con respecto a hoy


La lógica del método 'update_quality' la pondría dentro de Item. Hay mucho codigo espaguetti, por ejemplo en la parte de concierto hay 2 ifs seguidos para sumar 1 si la fecha es menor a 10 y otro 1 si es menor a 5, se podrian combinar más elegantemente y sumar el numero que hiciera falta solo 1 vez


SEMANTICA:
La clase GlidedRose la llamaría GestorInventario
La clase Item la llamaria Vendible